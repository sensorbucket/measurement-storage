#!/bin/bash

# Create a file called `.env` with the following contents:
#   DATABASE=postgres://username:password@localhost:5432/measurements
#   AMQP=amqp://username:password@localhost:5672/
source .env
nodemon --delay 0.2 --signal SIGINT -e go --exec 'go run main.go || exit 1'
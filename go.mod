module gitlab.com/sensorbucket/measurement-storage

go 1.15

require (
	github.com/go-chi/chi v1.5.3
	github.com/gofrs/uuid v4.0.0+incompatible // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/streadway/amqp v1.0.0
	github.com/twpayne/go-geom v1.4.0
	gitlab.com/sensorbucket/go-worker v0.0.2-0.20210512101010-45def9928c6d
	gitlab.com/sensorbucket/identity v0.0.5-0.20210517130747-e850333d89db // indirect
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.21.7
)

package main

import (
	"context"
	"flag"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/sensorbucket/identity/pkg/keystore"
	"gitlab.com/sensorbucket/identity/pkg/web"
	"gitlab.com/sensorbucket/measurement-storage/pkg/amqpses"
	"gitlab.com/sensorbucket/measurement-storage/pkg/flagenv"
	"gitlab.com/sensorbucket/measurement-storage/service"
	"gitlab.com/sensorbucket/measurement-storage/service/adapters/gormadapter"
	"gitlab.com/sensorbucket/measurement-storage/service/adapters/httpadapter"
	"gitlab.com/sensorbucket/measurement-storage/service/adapters/mqadapter"
	"gitlab.com/sensorbucket/measurement-storage/service/facade"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var (
	databaseString = flagenv.String("DB_URL", "db", "postgresql://localhost/measurementstorage", "Set the database connection string")
	mgmtUrl        = flagenv.String("MANAGEMENT_URL", "mangement", "http://device-management", "Set the device-management service URL")
	idenUrl        = flagenv.String("IDENTITY_URL", "identity", "http://identity", "Set the identity service URL")
	amqpString     = flagenv.String("AMQP_URL", "amqp", "amqp://localhost:5672", "Set the AMQP connection string")
	amqpQueue      = flagenv.String("AMQP_QUEUE", "amqp-queue", "measurement-storage", "Sets the queue to fetch measurements from")
	jwksUrl        = flagenv.String("JWKS_VERIFYING", "jwks", "http://key-management/public/sensorbucket", "The location of the private JWKS")
	appEnv         = flagenv.String("APP_ENV", "env", "development", "Set the app environment, either: development or production")
)

func main() {
	var err error

	flag.Parse()
	isProduction := *appEnv == "production"
	// Configure logging
	log := &logrus.Logger{
		Out:       os.Stdout,
		Level:     logrus.DebugLevel,
		Formatter: new(logrus.TextFormatter),
	}
	if isProduction {
		log.Level = logrus.WarnLevel
		log.Formatter = new(logrus.JSONFormatter)
	}

	ctx, ctxCancel := context.WithCancel(context.Background())
	wg := &sync.WaitGroup{}
	// Catch os signals
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGHUP, syscall.SIGINT, syscall.SIGQUIT)

	// Create HTTP server
	srv := web.NewServer("0.0.0.0", 3000)
	srvErr := srv.Listen()

	// Instantiat keystore
	ks := keystore.NewJWKSKeystore(*jwksUrl)
	err = ks.Update()
	if err != nil {
		logrus.WithError(err).Error("could not update keystore!")
		os.Exit(1)
	}

	// Create DB connection
	db, err := gorm.Open(postgres.Open(*databaseString))
	if err != nil {
		log.Fatalf("Database connection failed: %s", err)
	}
	log.Info("Database connection established")

	// Instantiate store
	store := gormadapter.New(log, db)
	// Instantiate facade
	f := facade.New(*mgmtUrl, *idenUrl)
	// Instantiate service
	svc := service.New(ctx, log, store, f)

	// Initialize HTTPAdapter
	httpa := httpadapter.New(svc, ks)
	srv.Mount("/", httpa.Routes())

	// Initialize AMQP
	mq := amqpses.New(ctx, wg, log.WithField("pkg", "amqpses"), *amqpString)
	mq.SetDeclaration(func(conn *amqp.Connection, ch *amqp.Channel) error {
		_, err := ch.QueueDeclare(*amqpQueue, true, false, false, false, nil)
		return err
	})
	wg.Add(1)
	go mq.Start()
	log.Info("AMQP Session created")

	// Initialize consumers and instantiate
	consumer := mq.CreateConsumer(*amqpQueue)
	// Initialize mqadapter
	mqa := mqadapter.New(ctx, log, svc, consumer)
	go consumer.Start()
	go mqa.Start()

	// Wait for an error or signal
	log.Info("Started, waiting for exit signal to shut down")
	select {
	// Exit on HTTP server error
	case err := <-srvErr:
		logrus.WithError(err).Error("HTTP Server fatal error")
	// Exit on exit signal
	case <-sigChan:
	}

	// Shutdown and clean up
	log.Info("Shutting down...")
	ctxCancel()
	wg.Done()
}

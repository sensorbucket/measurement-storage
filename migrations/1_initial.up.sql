CREATE EXTENSION IF NOT EXISTS postgis;
CREATE EXTENSION IF NOT EXISTS timescaledb;

CREATE TABLE measurements
(
  "id"                           BIGSERIAL,
  "timestamp"                    TIMESTAMP WITH TIME ZONE NOT NULL,
  "latitude"                     NUMERIC,
  "longitude"                    NUMERIC,
  "value"                        NUMERIC(10,4),
  "device_id"                    INTEGER,
  "device_description"           VARCHAR(128),
  "device_type_id"               VARCHAR,
  "device_type_description"      VARCHAR(75),
  "device_type_mobile"           BOOLEAN,
  "measurement_type_id"          INTEGER,
  "measurement_type_name"        VARCHAR(25),
  "measurement_type_description" VARCHAR(75),
  "measurement_type_unit"        VARCHAR(12),
  "organisation_id"              INTEGER,
  "orgnaisation_name"            VARCHAR(75),
  "organisation_address"         VARCHAR(50),
  "organisation_zipcode"         VARCHAR(7),
  "organisation_city"            VARCHAR(50),
  "location_id"                  INTEGER,
  "location_name"                VARCHAR(50),
  "location_address"             VARCHAR(50),
  "location_city"                VARCHAR(50)
);

SELECT create_hypertable('measurements', 'timestamp');
package mqadapter

import (
	"context"
	"encoding/json"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/sensorbucket/measurement-storage/pkg/amqpses"
	"gitlab.com/sensorbucket/measurement-storage/service"
)

// MQInlet ...
type MQAdapter struct {
	ctx      context.Context
	log      *logrus.Entry
	svc      service.Service
	consumer *amqpses.Consumer
}

func New(ctx context.Context, log *logrus.Logger, svc service.Service, consumer *amqpses.Consumer) *MQAdapter {
	return &MQAdapter{
		ctx:      ctx,
		log:      log.WithField("pkg", "mqadapter"),
		svc:      svc,
		consumer: consumer,
	}
}

// Start is a blocking function which will start consuming MQ messages
func (a *MQAdapter) Start() {
	defer a.log.Info("MQAdapter exited")
	deliveries := a.consumer.Deliveries()
	for {
		select {
		case <-a.ctx.Done():
			return
		case delivery := <-deliveries:
			a.handleDelivery(delivery)
		}
	}
}

func (a *MQAdapter) handleDelivery(d amqp.Delivery) {
	var msg service.PipelineMeasurementMessage

	// Unmarshal pipeline message
	err := json.Unmarshal(d.Body, &msg)
	if err != nil {
		a.log.WithError(err).Error("Could not unmarshal pipeline message")
		return
	}

	// Try to insert it
	err = a.svc.InsertMeasurement(&msg)
	if err != nil {
		a.log.WithField("id", msg.ID).WithError(err).Error("failed to insert measurement")
		if d.Redelivered {
			d.Nack(false, false)
		}
		return
	}

	a.log.WithField("id", msg.ID).Info("Parsed measurement message")
	d.Ack(false)
}

package httpadapter

import (
	"gitlab.com/sensorbucket/identity/pkg/keystore"
	"gitlab.com/sensorbucket/measurement-storage/service"
)

// HTTPAdapter ...
type HTTPAdapter struct {
	svc      service.Service
	keystore keystore.Keyprovider
}

func New(svc service.Service, keystore keystore.Keyprovider) *HTTPAdapter {
	return &HTTPAdapter{
		svc:      svc,
		keystore: keystore,
	}
}

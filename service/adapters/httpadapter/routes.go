package httpadapter

import (
	"net/http"
	"strconv"
	"time"

	"github.com/go-chi/chi"
	"gitlab.com/sensorbucket/identity/pkg/web"
	"gitlab.com/sensorbucket/identity/pkg/webauth"
)

func (a *HTTPAdapter) httpGetMeasurements() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		// Get user ID from request context
		// ctxToken, _ := r.Context().Value(webauth.CTXTokenKey).(webauth.Token)

		// Get query parameters
		measurementQ := r.URL.Query().Get("measurement")
		locationQ := r.URL.Query().Get("location")
		startQ := r.URL.Query().Get("start")
		endQ := r.URL.Query().Get("end")

		// Verify that parameters are set
		if measurementQ == "" {
			web.HTTPError(rw, &web.APIError{
				Code:       "MISSING_PARAMETER",
				Message:    "Missing `measurement` query parameter",
				HTTPStatus: 400,
			})
			return
		}
		if locationQ == "" {
			web.HTTPError(rw, &web.APIError{
				Code:       "MISSING_PARAMETER",
				Message:    "Missing `location` query parameter",
				HTTPStatus: 400,
			})
			return
		}
		if startQ == "" {
			web.HTTPError(rw, &web.APIError{
				Code:       "MISSING_PARAMETER",
				Message:    "Missing `start` query parameter",
				HTTPStatus: 400,
			})
			return
		}
		if endQ == "" {
			web.HTTPError(rw, &web.APIError{
				Code:       "MISSING_PARAMETER",
				Message:    "Missing `end` query parameter",
				HTTPStatus: 400,
			})
			return
		}

		// Convert parameters
		measurementID, err := strconv.ParseInt(measurementQ, 10, 32)
		if err != nil {
			web.HTTPError(rw, &web.APIError{
				Code:       "INVALID_PARAMETER",
				Message:    "Invalid `measurement` query parameter",
				HTTPStatus: 400,
			})
			return
		}
		locationID, err := strconv.ParseInt(locationQ, 10, 32)
		if err != nil {
			web.HTTPError(rw, &web.APIError{
				Code:       "INVALID_PARAMETER",
				Message:    "Invalid `location` query parameter",
				HTTPStatus: 400,
			})
			return
		}
		start, err := time.Parse(time.RFC3339, startQ)
		if err != nil {
			web.HTTPError(rw, &web.APIError{
				Code:       "INVALID_PARAMETER",
				Message:    "Invalid `start` query parameter",
				HTTPStatus: 400,
			})
			return
		}
		end, err := time.Parse(time.RFC3339, endQ)
		if err != nil {
			web.HTTPError(rw, &web.APIError{
				Code:       "INVALID_PARAMETER",
				Message:    "Invalid `end` query parameter",
				HTTPStatus: 400,
			})
			return
		}

		m, err := a.svc.GetMeasurements(int(locationID), int(measurementID), start, end)
		if err != nil {
			web.HTTPError(rw, &web.APIError{
				Code:       "INTERNAL_ERROR",
				Message:    "Could not fetch measurements",
				HTTPStatus: 500,
			})
			return
		}

		// Respond with user id
		web.HTTPResponse(rw, http.StatusOK, &web.APIResponse{
			Message: "Hello world",
			Data:    m,
		})
	}
}

// Routes creates router for the user routes
func (a *HTTPAdapter) Routes() *chi.Mux {
	r := chi.NewRouter()
	auth := webauth.Create(a.keystore)

	// Protected routes
	r.Group(func(pr chi.Router) {
		pr.Use(auth)
		pr.Get("/measurements", a.httpGetMeasurements())
	})

	return r
}

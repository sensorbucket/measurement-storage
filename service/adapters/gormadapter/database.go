package gormadapter

import (
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/sensorbucket/measurement-storage/service"
	"gorm.io/gorm"
)

// db ...
type gormAdapter struct {
	log *logrus.Entry
	db  *gorm.DB
}

func New(log *logrus.Logger, db *gorm.DB) service.Store {
	return &gormAdapter{
		log: log.WithField("pkg", "gormadapter"),
		db:  db,
	}
}

func (a *gormAdapter) InsertMeasurement(m *service.Measurement) error {
	// a.db.AutoMigrate(&service.Measurement{})
	res := a.db.Create(m)
	return res.Error
}

func (a *gormAdapter) GetMeasurementsInTimerange(orgID, locID, measurementType int, startTime, endTime time.Time) ([]*service.Measurement, error) {
	measurements := make([]*service.Measurement, 0)
	res := a.db.Where(&service.Measurement{
		OrganisationID:    orgID,
		LocationID:        locID,
		MeasurementTypeID: measurementType,
	}).Where(
		"timestamp > ? AND timestamp < ?",
		startTime,
		endTime,
	).Find(&measurements)

	return measurements, res.Error
}

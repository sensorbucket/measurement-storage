package facade

import (
	"errors"

	geom "github.com/twpayne/go-geom"
	"github.com/twpayne/go-geom/encoding/geojson"
)

// Device ...
type Device struct {
	ID          int    `json:"id,omitempty"`
	Code        string `json:"code,omitempty"`
	Description string `json:"description,omitempty"`
	TypeID      string `json:"typeId,omitempty"`
	SourceID    string `json:"sourceId,omitempty"`
	OwnerID     int    `json:"ownerId,omitempty"`
	LocationID  *int   `json:"locationId,omitempty"`
}

// DeviceType ...
type DeviceType struct {
	ID          string `json:"id,omitempty"`
	Name        string `json:"name,omitempty"`
	Description string `json:"description,omitempty"`
	Mobile      bool   `json:"mobile,omitempty"`
}

// Source ...
type Source struct {
	ID          string `json:"id,omitempty"`
	Name        string `json:"name,omitempty"`
	Description string `json:"description,omitempty"`
}

// Organisation ...
type Organisation struct {
	ID          int    `json:"id,omitempty"`
	Name        string `json:"name,omitempty"`
	Address     string `json:"address,omitempty"`
	Zipcode     string `json:"zipcode,omitempty"`
	City        string `json:"city,omitempty"`
	ArchiveTime int    `json:"archiveTime,omitempty"`
}

// MeasurementType ...
type MeasurementType struct {
	ID          int    `json:"id,omitempty"`
	Name        string `json:"name,omitempty"`
	Description string `json:"description,omitempty"`
	Unit        string `json:"unit,omitempty"`
}

type Point geom.Point

func (p *Point) UnmarshalJSON(data []byte) error {
	var g geom.T

	err := geojson.Unmarshal(data, &g)
	if err != nil {
		return err
	}

	// Check if it really is a point
	pnt, isPoint := g.(*geom.Point)
	if !isPoint {
		return errors.New("GeoJSON does not represent a Point")
	}

	*p = Point(*pnt)
	return nil
}

// Location ...
type Location struct {
	ID          int `json:"id,omitempty"`
	Name        string
	Description string
	Coordinates Point
}

// ==============
// API Responses
// ==============
// Declaring them like this will save us from mapping a map[string]interface{}
// to the corresponding struct.

// APIResponse a generic api response
type APIResponse struct {
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

// APIResponseDevice ...
type APIResponseDevice struct {
	Data *Device `json:"data"`
	APIResponse
}

// APIResponseDevice ...
type APIResponseDeviceType struct {
	Data *DeviceType `json:"data"`
	APIResponse
}

// APIResponseDevice ...
type APIResponseSource struct {
	Data *Source `json:"data"`
	APIResponse
}

// APIResponseLocation ...
type APIResponseLocation struct {
	Data *Location `json:"data"`
	APIResponse
}

// APIResponseOrganisation ...
type APIResponseOrganisation struct {
	Data *Organisation `json:"data"`
	APIResponse
}

// APIResponseMeasurementType ...
type APIResponseMeasurementType struct {
	Data *MeasurementType `json:"data"`
	APIResponse
}

package facade

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"
)

// Facade represents the management service
type Facade interface {
	GetDevice(id int, authToken string) (*Device, error)
	GetDeviceType(id string, authToken string) (*DeviceType, error)
	GetSource(id string, authToken string) (*Source, error)
	GetLocation(id int, authToken string) (*Location, error)
	GetOrganisation(id int, authToken string) (*Organisation, error)
	GetMeasurementType(id int, authToken string) (*MeasurementType, error)
}

// FacadeImpl ...
type FacadeImpl struct {
	mgmtUrl string
	idenUrl string
}

var (
	// ErrResourceNotFound ...
	ErrResourceNotFound = errors.New("The requested resource was not found")
	// ErrGenericError ...
	ErrGenericError = errors.New("The server responded with an error")
)

var client = &http.Client{
	Timeout: 5 * time.Second,
}

// New ...
func New(mgmtUrl, idenUrl string) Facade {
	return &FacadeImpl{
		mgmtUrl: mgmtUrl,
		idenUrl: idenUrl,
	}
}

func (f *FacadeImpl) getResource(url, authToken string, v interface{}) error {
	res, err := f.req(url, authToken)
	if err != nil {
		return err
	}

	// Verify response code
	if res.StatusCode < 200 || res.StatusCode > 299 {
		switch res.StatusCode {
		case 404:
			return fmt.Errorf("%w: %d %s", ErrResourceNotFound, res.StatusCode, url)
		default:
			return fmt.Errorf("%w: %d %s", ErrGenericError, res.StatusCode, url)
		}
	}

	if err := json.NewDecoder(res.Body).Decode(v); err != nil {
		return err
	}

	return nil
}

func (f *FacadeImpl) req(url, authToken string) (*http.Response, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Authorization", fmt.Sprintf("bearer %s", authToken))

	return client.Do(req)
}

// GetDevice ...
func (f *FacadeImpl) GetDevice(id int, authToken string) (*Device, error) {
	var res APIResponseDevice
	err := f.getResource(fmt.Sprintf("%s/devices/%d", f.mgmtUrl, id), authToken, &res)
	return res.Data, err
}

// GetDeviceType ...
func (f *FacadeImpl) GetDeviceType(id string, authToken string) (*DeviceType, error) {
	var res APIResponseDeviceType
	err := f.getResource(fmt.Sprintf("%s/device-types/%s", f.mgmtUrl, id), authToken, &res)
	return res.Data, err
}

// GetSource ...
func (f *FacadeImpl) GetSource(id string, authToken string) (*Source, error) {
	var res APIResponseSource
	err := f.getResource(fmt.Sprintf("%s/sources/%s", f.mgmtUrl, id), authToken, &res)
	return res.Data, err
}

// GetLocation ...
func (f *FacadeImpl) GetLocation(id int, authToken string) (*Location, error) {
	var res APIResponseLocation
	err := f.getResource(fmt.Sprintf("%s/locations/%d", f.mgmtUrl, id), authToken, &res)
	return res.Data, err
}

// GetOrganisation ...
func (f *FacadeImpl) GetOrganisation(id int, authToken string) (*Organisation, error) {
	var res APIResponseOrganisation
	err := f.getResource(fmt.Sprintf("%s/organisations/%d", f.idenUrl, id), authToken, &res)
	return res.Data, err
}

// GetMeasurementType ...
func (f *FacadeImpl) GetMeasurementType(id int, authToken string) (*MeasurementType, error) {
	var res APIResponseMeasurementType
	err := f.getResource(fmt.Sprintf("%s/measurement-types/%d", f.mgmtUrl, id), authToken, &res)
	return res.Data, err
}

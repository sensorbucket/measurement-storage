package service

import "time"

// Service ...
type Service interface {
	InsertMeasurement(*PipelineMeasurementMessage) error
	GetMeasurements(locID, measurementType int, startTime, endTime time.Time) ([]*Measurement, error)
}

// Store ...
type Store interface {
	InsertMeasurement(*Measurement) error
	GetMeasurementsInTimerange(orgID, locID, measurementType int, startTime, endTime time.Time) ([]*Measurement, error)
}

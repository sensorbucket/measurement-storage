package service

import (
	"context"
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/sensorbucket/measurement-storage/service/facade"
)

// Config ...
type Config struct {
	DatabaseString string
	AMQPString     string
	AMQPQueue      string
	IsProduction   bool
}

// logic ...
type logic struct {
	ctx    context.Context
	log    *logrus.Entry
	store  Store
	facade facade.Facade
}

// Start ...
func New(ctx context.Context, log *logrus.Logger, store Store, f facade.Facade) Service {
	return &logic{
		ctx:    ctx,
		log:    log.WithField("pkg", "service"),
		store:  store,
		facade: f,
	}
}

func (l *logic) InsertMeasurement(msg *PipelineMeasurementMessage) error {
	// Gather relevant data from services
	dev, err := l.facade.GetDevice(msg.Device.ID, msg.Token)
	if err != nil {
		return fmt.Errorf("could not retrieve device data from management service: %w", err)
	}

	typ, err := l.facade.GetDeviceType(dev.TypeID, msg.Token)
	if err != nil {
		return fmt.Errorf("could not retrieve device type from management service: %w", err)
	}

	// src, err := l.facade.GetSource(dev.SourceID, msg.Token)
	// if err != nil {
	// 	return fmt.Errorf("could not retrieve source from management service: %w", err)
	// }

	org, err := l.facade.GetOrganisation(dev.OwnerID, msg.Token)
	if err != nil {
		return fmt.Errorf("could not retrieve organisation data from identity service: %w", err)
	}

	var loc *facade.Location = nil
	if dev.LocationID != nil {
		loc, err = l.facade.GetLocation(*dev.LocationID, msg.Token)
		if err != nil {
			return fmt.Errorf("could not retrieve location data from management service: %w", err)
		}
	}

	for ix := 0; ix < len(msg.Payload); ix++ {
		in := msg.Payload[ix]

		mTyp, err := l.facade.GetMeasurementType(in.ID, msg.Token)
		if err != nil {
			l.log.WithError(err).Error("Could not fetch measurement type for measurement")
			continue
		}

		measurement := &Measurement{
			Timestamp: msg.DateTime,

			Value:                      in.Value,
			MeasurementTypeID:          mTyp.ID,
			MeasurementTypeName:        mTyp.Name,
			MeasurementTypeDescription: mTyp.Description,
			MeasurementTypeUnit:        mTyp.Unit,

			DeviceID:          dev.ID,
			DeviceDescription: dev.Description,

			DeviceTypeID:          typ.ID,
			DeviceTypeDescription: typ.Description,
			DeviceTypeMobile:      typ.Mobile,

			OrganisationID:      org.ID,
			OrgnaisationName:    org.Name,
			OrganisationCity:    org.City,
			OrganisationAddress: org.Address,
			OrganisationZipcode: org.Zipcode,
		}

		// Add location if exists
		if loc != nil {
			measurement.LocationID = loc.ID
			measurement.LocationName = loc.Name
			measurement.Latitude = loc.Coordinates.Coords().X()
			measurement.Longitude = loc.Coordinates.Coords().Y()
		}

		err = l.store.InsertMeasurement(measurement)
		if err != nil {
			l.log.WithError(err).Error("Could not insert measurement to store")
		}
	}

	l.log.WithFields(logrus.Fields{
		"dev": dev.ID,
		"org": org.Name,
	}).Info("Fetched relevant data")

	return nil
}

func (l *logic) GetMeasurements(locID, measurementType int, startTime, endTime time.Time) ([]*Measurement, error) {
	return l.store.GetMeasurementsInTimerange(1, locID, measurementType, startTime, endTime)
}

package service

import (
	"time"

	"gitlab.com/sensorbucket/go-worker/pipeline"
)

// Measurement ...
type Measurement struct {
	ID                         int       `json:"id"`
	Timestamp                  time.Time `json:"timestamp"`
	Latitude                   float64   `json:"latitude"`
	Longitude                  float64   `json:"longitude"`
	Value                      float32   `json:"value"`
	DeviceID                   int       `json:"deviceId"`
	DeviceDescription          string    `json:"deviceDescription"`
	DeviceTypeID               string    `json:"deviceTypeId"`
	DeviceTypeDescription      string    `json:"deviceTypeDescription"`
	DeviceTypeMobile           bool      `json:"deviceTypeMobile"`
	MeasurementTypeID          int       `json:"measurementTypeId"`
	MeasurementTypeName        string    `json:"measurementTypeName"`
	MeasurementTypeDescription string    `json:"measurementTypeDescription"`
	MeasurementTypeUnit        string    `json:"measurementTypeUnit"`
	OrganisationID             int       `json:"orgnisationId"`
	OrgnaisationName           string    `json:"orgnaisationName"`
	OrganisationAddress        string    `json:"organisationAddress"`
	OrganisationZipcode        string    `json:"organisationZipcode"`
	OrganisationCity           string    `json:"organisationCity"`
	LocationID                 int       `json:"locationId"`
	LocationName               string    `json:"locationName"`
	LocationAddress            string    `json:"locationAddress"`
	LocationCity               string    `json:"locationCity"`
}

// MeasurementInsert ...
type MeasurementInsert struct {
	ID          int    `json:"id,omitempty"`
	Timestamp   string `json:"timestamp,omitempty"`
	Coordinates struct {
		Latitude  int `json:"latitude,omitempty"`
		Longitude int `json:"longitude,omitempty"`
	} `json:"coordinates,omitempty"`
	Value    int `json:"value,omitempty"`
	DeviceID int `json:"deviceId,omitempty"`
	// DeviceDescription     string `json:"deviceDescription,omitempty"`
	DeviceTypeID int `json:"deviceTypeId,omitempty"`
	// DeviceTypeDescription string `json:"deviceTypeDescription,omitempty"`
	// DeviceTypeMobile      bool   `json:"deviceTypeMobile,omitempty"`
	UnitID int `json:"unitId,omitempty"`
	// UnitDescription       string `json:"unitDescription,omitempty"`
	// UnitValue             string `json:"unitValue,omitempty"`
	OrgnisationID int `json:"orgnisationId,omitempty"`
	// OrgnaisationName      string `json:"orgnaisationName,omitempty"`
	// OrganisationAddress   string `json:"organisationAddress,omitempty"`
	// OrganisationZipcode   string `json:"organisationZipcode,omitempty"`
	// OrganisationCity      string `json:"organisationCity,omitempty"`
	LocationID int `json:"locationId,omitempty"`
	// LocationName          string `json:"locationName,omitempty"`
	// LocationAddress       string `json:"locationAddress,omitempty"`
	// LocationCity          string `json:"locationCity,omitempty"`
}

// PipelineMeasurement ...
type PipelineMeasurement struct {
	ID    int     `json:"id,omitempty"`
	Value float32 `json:"value,omitempty"`
}
type PipelineMeasurementMessage struct {
	pipeline.NodeMessage
	Payload []PipelineMeasurement `json:"payload,omitempty"`
}

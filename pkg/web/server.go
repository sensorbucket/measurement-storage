package web

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/go-chi/chi"
)

// Config contains http configuration
type Config struct {
	Addr string
	Port int
}

// Server represents the server config
type Server struct {
	Router    chi.Router
	HTTP      *http.Server
	isHealthy bool
}

// HTTP Handler used to indicate server health status
func (s *Server) getHealth() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		if s.isHealthy {
			rw.WriteHeader(http.StatusOK)
			rw.Write([]byte("healthy"))
		} else {
			rw.WriteHeader(http.StatusServiceUnavailable)
			rw.Write([]byte("unhealthy"))
		}
	}
}

// NewServer creates a new server
func NewServer(bind string, port int) *Server {
	r := chi.NewRouter()
	h := &http.Server{
		Handler:      r,
		Addr:         fmt.Sprintf("%s:%d", bind, port),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	s := &Server{
		Router:    r,
		HTTP:      h,
		isHealthy: false,
	}

	return s
}

// Listen starts listening on the set address
func (s *Server) Listen() chan error {

	// Configure health check
	s.Router.Get("/health", s.getHealth())

	errC := make(chan error, 1)
	go func() {
		if err := s.HTTP.ListenAndServe(); err != http.ErrServerClosed {
			errC <- err
		}
	}()
	return errC
}

// Shutdown stops the server
func (s *Server) Shutdown(ctx context.Context) error {
	err := s.HTTP.Shutdown(ctx)
	return err
}

// Mount a subhandler
func (s *Server) Mount(pattern string, h http.Handler) {
	s.Router.Mount(pattern, h)
}

// SetHealthy sets the healthy status for the server
func (s *Server) SetHealthy(healthy bool) {
	s.isHealthy = healthy
}

// IsHealthy gets the healthy status from the server
func (s *Server) IsHealthy() bool {
	return s.isHealthy
}

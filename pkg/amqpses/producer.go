package amqpses

import (
	"context"
	"errors"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

// Producer ...
type Producer struct {
	ctx   context.Context
	wg    *sync.WaitGroup
	mq    *Session
	xchg  string
	queue chan *Publishing
}

// Publishing ...
type Publishing struct {
	Message amqp.Publishing
	Route   string
}

func (p *Producer) Start() {
	defer p.wg.Done()
	defer logrus.Info("Producer stopped")

	p.connectionHandler()
}

func (p *Producer) connectionHandler() {
	var err error
	for {
		// Check and wait until mq is connected
		// Here we wait for the "shared resource" IsConnected() to become available
		// again, in this case if
		p.mq.WaitForConnection()

		// Check if we're cancelled (ctx.Done) otherwise reconnect
		select {
		case <-p.ctx.Done():
			return

		// Reconnect
		default:
			// if NO error occured then we are supposed to exit because ctx is cancelled
			if err = p.publishHandler(); err == nil {
				return
			}
		}

		logrus.WithError(err).Warn("Producer died, reconnecting")
		select {
		case <-p.ctx.Done():
			return
		case <-time.After(retryTimeout):
		}
	}
}

func (p *Producer) Produce(route string, publishing amqp.Publishing) {
	// Queue the new message
	p.queue <- &Publishing{
		Message: publishing,
		Route:   route,
	}
}

func (p *Producer) publishHandler() error {
	// Get amqp Channel
	ch := p.mq.Channel()
	if ch == nil {
		return errors.New("AMQP channel not available (nil)")
	}
	logrus.Info("Producer connected")

	for {
		select {
		// Exit if closing
		case <-p.ctx.Done():
			return nil
		case publishing, ok := <-p.queue:
			if !ok {
				return nil
			}

			// Publish the item
			err := ch.Publish(p.xchg, publishing.Route, false, false, publishing.Message)
			if err != nil {
				return err
			}
		}
	}
}

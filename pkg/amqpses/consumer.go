package amqpses

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

var (
	retryTimeout = 1 * time.Second
)

// Consumer ...
type Consumer struct {
	ctx        context.Context
	wg         *sync.WaitGroup
	mq         *Session
	queue      string
	deliveries chan amqp.Delivery
}

// Start is a blocking call which starts the consumer and handles reconnects
func (c *Consumer) Start() {
	var err error
	defer c.wg.Done()
	defer logrus.Info("Consumer stopped")

	for {
		// Check and wait until mq is connected
		// Here we wait for the "shared resource" IsConnected() to become available
		// again, in this case if
		c.mq.WaitForConnection()

		// Check if we're cancelled (ctx.Done) otherwise reconnect
		select {
		case <-c.ctx.Done():
			return

		// Reconnect
		default:
			err = c.connect()
			// if NO error occured then we are supposed to exit because ctx is cancelled
			if err == nil {
				return
			}
		}

		logrus.WithError(err).Warn("Consumer died, reconnecting")
		select {
		case <-c.ctx.Done():
			return
		case <-time.After(retryTimeout):
		}
	}
}

func (c *Consumer) connect() error {
	// Get amqp Channel
	ch := c.mq.Channel()
	if ch == nil {
		return errors.New("AMQP channel not available (nil)")
	}

	// Create consumer
	deliveries, err := ch.Consume(c.queue, "", false, false, false, false, amqp.Table{})
	if err != nil {
		return fmt.Errorf("could not consume from amqp channel: %w", err)
	}

	logrus.Info("Consumer connected")

	// Start delivery handler which delegates to our persistent channel
	return c.deliveryHandler(deliveries)
}

// delegates deliveries to the deliveries channel
func (c *Consumer) deliveryHandler(deliveries <-chan amqp.Delivery) error {
	for {
		select {
		// Exit if context is done
		case <-c.ctx.Done():
			return nil
		// Delegate message or handle error on channel
		case delivery, ok := <-deliveries:
			if !ok {
				return errors.New("amqp delivery channel closed")
			}
			c.deliveries <- delivery
		}
	}
}

func (c *Consumer) Deliveries() <-chan amqp.Delivery {
	return c.deliveries
}

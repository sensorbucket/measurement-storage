package amqpses

import (
	"context"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

// ChanSetupFn is a function that is called when a new channel must be set up
type ChanSetupFn func(c *amqp.Channel) error

const (
	reconnectDelay        = 5 * time.Second
	producerQueueCapacity = 1000
)

// DeclarationFunc is fired on a new connection
// if this function fails, it assumes the reconnection has failed
type DeclarationFunc func(*amqp.Connection, *amqp.Channel) error

// Status represents the current status of the message queue connection
type Status int

const (
	// StatusConnected means a connection to the MQ is present
	StatusConnected Status = iota
	// StatusDisconnected means the connection is lost
	StatusDisconnected
)

// Session ...
type Session struct {
	ctx             context.Context
	wg              *sync.WaitGroup
	log             *logrus.Entry
	done            chan struct{}
	addr            string
	declarationFunc DeclarationFunc

	// connection specific variables, change when reconnected
	connection    *amqp.Connection
	channel       *amqp.Channel
	notifyClose   chan *amqp.Error
	notifyConfirm chan amqp.Confirmation

	// wg waits for producers / consumers to exit
	pcWg *sync.WaitGroup

	// locked while reconnecting to the queue
	connCond *sync.Cond
	// true if currently connected
	isConnected bool
}

// New ...
func New(ctx context.Context, wg *sync.WaitGroup, log *logrus.Entry, addr string) *Session {
	return &Session{
		ctx:      ctx,
		wg:       wg,
		done:     make(chan struct{}),
		pcWg:     &sync.WaitGroup{},
		log:      log,
		addr:     addr,
		connCond: sync.NewCond(&sync.Mutex{}),
	}
}

// Start is a blocking function which starts the (re)connection handler
func (s *Session) Start() {
	// Signal done after this function ends
	defer s.wg.Done()

	// Start connection handler
	go s.handleReconnect(s.addr)

	// Wait for cancellation
	<-s.ctx.Done()

	// Signal producers / consumers to stop and wait for them
	s.pcWg.Wait()
	// Stop connection handler
	close(s.done)
	s.setConnected(false)

	// Close any active connections
	if err := s.channel.Close(); err != nil {
		s.log.WithError(err).Warn("AMQP Channel close failed")
	}
	if err := s.connection.Close(); err != nil {
		s.log.WithError(err).Warn("AMQP Connection close failed")
	}
}

func (s *Session) setConnected(connected bool) {
	s.connCond.L.Lock()
	s.isConnected = connected
	s.connCond.L.Unlock()
	s.connCond.Broadcast()
}

// SetDeclaration sets the function that will assert the topology
func (s *Session) SetDeclaration(fn DeclarationFunc) {
	s.declarationFunc = fn
}

func (s *Session) handleReconnect(addr string) {
	for {
		s.log.Info("Attempting AMQP connection")

		// Attempt reconnection until
		retryCount := 0
		for !s.connect(addr) {
			// During the reconnect delay we might close the connection
			// so catch <-s.ctx.Done()
			select {
			// Fired when closing
			case <-s.done:
				return

			// Fires after x seconds
			case <-time.After(reconnectDelay + time.Duration(retryCount)*time.Second):
				retryCount++
				s.log.WithField("retry", retryCount).Info("Attempting reconnect")
			}
		}

		// Connection succeeded so unlock
		s.log.Info("AMQP Reconnected")

		// Wait for exit or error
		select {
		case <-s.done:
			return
		case err := <-s.notifyClose:
			s.setConnected(false)
			s.log.WithError(err).Warn("AMQP Disconnected")
		}
	}
}

// Connect ...
func (s *Session) connect(addr string) bool {
	conn, err := amqp.Dial(addr)
	if err != nil {
		s.log.WithError(err).Warn("AMQP Connection failed")
		return false
	}

	ch, err := conn.Channel()
	if err != nil {
		s.log.WithError(err).Warn("AMQP Channel creation failed")
		return false
	}

	if s.declarationFunc != nil {
		err = s.declarationFunc(conn, ch)
		if err != nil {
			s.log.WithError(err).Warn("AMQP Declaration failed")
			return false
		}
	}

	s.changeConnection(conn, ch)
	s.setConnected(true)

	return true
}

func (s *Session) changeConnection(conn *amqp.Connection, ch *amqp.Channel) {
	s.connection = conn
	s.channel = ch
	s.notifyClose = make(chan *amqp.Error)
	s.notifyConfirm = make(chan amqp.Confirmation)
	ch.NotifyClose(s.notifyClose)
	ch.NotifyPublish(s.notifyConfirm)
}

func (s *Session) CreateConsumer(queue string) *Consumer {
	s.pcWg.Add(1)
	return &Consumer{
		ctx:        s.ctx,
		wg:         s.pcWg,
		mq:         s,
		queue:      queue,
		deliveries: make(chan amqp.Delivery),
	}
}

func (s *Session) CreateProducer(exchange string) *Producer {
	s.pcWg.Add(1)
	return &Producer{
		ctx:   s.ctx,
		wg:    s.pcWg,
		mq:    s,
		xchg:  exchange,
		queue: make(chan *Publishing, producerQueueCapacity),
	}
}

// Channel returns the active channel
func (s *Session) Channel() *amqp.Channel {
	return s.channel
}

func (s *Session) IsConnected() bool {
	return s.isConnected
}

func (s *Session) WaitForConnection() {
	s.connCond.L.Lock()
	for !s.IsConnected() {
		s.connCond.Wait()
	}
	s.connCond.L.Unlock()
}
